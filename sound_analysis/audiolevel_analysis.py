import librosa
import numpy as np
import time
import datetime
import matplotlib.pylab as plt

from librosa import stft

# argv[1] =  video = path vers fichier video
# argv[2] =   sequencesFile = path vers fichier contenant les timecodes de chaque segment de video


# parses the sequences file into float array and returns it
def parseSceneTimecodes(sequencesFile):
    sequences = []
    with open(sequencesFile) as f:
        content = f.readlines()
        contentLen = len(content)
        for i in range(contentLen):
            timeArray = content[i].strip().split(':')
            sec = float(timeArray[0] * 3600 + timeArray[1] * 60 + timeArray[2])
            sequences.append(sec)
    return sequences


# tranform the audio into a matrix of short term fourier-coefficients
# get the magnitude of the coefficients
# get the root mean square of the magnitudeds
# return the average
def getLoudness(audio):
    S, phase = librosa.magphase(stft(audio))
    return np.average(librosa.feature.rms(S=S))

# get audio information from file
def getAudioInformations(file):
    audio, sampleRate = librosa.load(file)
    audioDuration = librosa.get_duration(y=audio, sr=sampleRate)
    return audio, sampleRate, audioDuration

def getSequencesLoudness(audio, sampleRate, audioDuration, sequences):
    # create array containing the indices of each sequence in the audio then split the audio using the indices
    seqsSamplesIndex = []
    for sequence in sequences:
        seqsSamplesIndex.append(int(sequence * sampleRate))
    audioSplit = np.split(audio, seqsSamplesIndex)

    # find the loudness for each of the sequences
    sequencesLoudness = []
    for i in audioSplit:
        sequencesLoudness.append(getLoudness(i))

    return sequencesLoudness


#  split an audio file using the sequence files provided then
#  compute and print average loudness for each of these sequences
def analyseLoudness(videoFile, sequencesFile):
    sequences = parseSceneTimecodes(sequencesFile)
    audio, sampleRate, audioDuration = getAudioInformations(videoFile)
    print("audio length : ", audioDuration)
    print("sample rate : ", sampleRate)
    
    sequencesLoudness = getSequencesLoudness(audio, sampleRate, audioDuration, sequences)
    currentSeq = 0
    for loudness in sequencesLoudness:
        startTime = "0:00" if currentSeq == 0 else str(sequences[currentSeq - 1])
        endTime = audioDuration if currentSeq == len(sequences) else str(sequences[currentSeq])
        print("sequence ", currentSeq, " (", startTime, " - ", endTime, ")", " average loudness : ", loudness)
        currentSeq += 1
