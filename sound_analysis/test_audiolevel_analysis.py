import unittest
import audiolevel_analysis as analysis


class TestAudioLevelAnlysis(unittest.TestCase):

    # test a known file see if the float array matches with what it should return
    def test_parse(self):
        expected_values = [1.0, 2.0]
        result = analysis.parseSceneTimecodes(
            "tests/three_sec_sequences.txt")
        self.assertEqual(result, expected_values)

    # test a known file and see if the differencce between the sequences are normal (- or +..)
    def test_crescendo_loudness_analysis(self):
        videoFile = "tests/1000hz_3sec_crescendo.mp3"
        sequencesFile = "tests/three_sec_sequences.txt"
        sequences = analysis.parseSceneTimecodes(sequencesFile)
        audio, sampleRate, audioDuration = analysis.getAudioInformations(videoFile)
        sequencesLoudness = analysis.getSequencesLoudness(audio, sampleRate, audioDuration, sequences)
        lastLoudness = sequencesLoudness[0]
        result = True
        for loudness in sequencesLoudness:
            if (lastLoudness > loudness):
                result = False
        self.assertTrue(result)

if __name__ == '__main__':
    unittest.main()

