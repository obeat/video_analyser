from PIL import Image
import sys
import cv2
import os

import numpy as np

def bincount_app(a):
    a2D = a.reshape(-1,a.shape[-1])
    col_range = (256, 256, 256) # generically : a2D.max(0)+1
    a1D = np.ravel_multi_index(a2D.T, col_range)
    return np.unravel_index(np.bincount(a1D).argmax(), col_range)

def getColorFreq(frame):
     
    frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
    pi = Image.fromarray(frame)
    
    w, h = pi.size
    pixels = pi.getcolors(w * h)

    most_frequent_pixel = pixels[0]

    for count, colour in pixels:
        if count > most_frequent_pixel[0]:
            most_frequent_pixel = (count, colour)

    return(most_frequent_pixel)

def main():
    r = g = b = count = 0
    video = sys.argv[1]
    vidcap = cv2.VideoCapture(video)
    while vidcap.isOpened():
        success, image = vidcap.read()
        if success:
            col = getColorFreq(image)
            r += col[1][0]
            g += col[1][1]
            b += col[1][2]
            count += 1
        else:
            break
    cv2.destroyAllWindows()
    vidcap.release()
    r = round(r / count, 0)
    g = round(g / count, 0)
    b = round(b / count, 0)
    print(r, g, b)

main()