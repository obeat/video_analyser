import sys
import os

sys.path.insert(1, './communication')
sys.path.insert(1, './tint_compare')
from receive_data import convert_vid
from videoTints import printVideoTint

def check_args():
    base64video = sys.stdin.readline()
    convert_vid(base64video.rstrip())
    os.system('./splitVid.sh')
    printVideoTint("jsp")
        
check_args()
