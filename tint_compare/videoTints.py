from PIL import Image
import sys
import cv2
import os

import numpy as np

def get_sec(time_str):
    if isinstance(time_str, str):
        h, m, s = time_str.split(':')
        return float(h) * 3600 + float(m) * 60 + float(s)
    return time_str

def bincount_app(a):
    a2D = a.reshape(-1,a.shape[-1])
    col_range = (256, 256, 256) # generically : a2D.max(0)+1
    a1D = np.ravel_multi_index(a2D.T, col_range)
    return np.unravel_index(np.bincount(a1D).argmax(), col_range)

def getColorFreq(frame):
     
    frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
    pi = Image.fromarray(frame)
    
    w, h = pi.size
    pixels = pi.getcolors(w * h)

    most_frequent_pixel = pixels[0]

    for count, colour in pixels:
        if count > most_frequent_pixel[0]:
            most_frequent_pixel = (count, colour)

    return(most_frequent_pixel)

def printVideoTint(jsp):
    if jsp:
        filepath = 'videoresult-Scenes.csv'
    
        with open(filepath) as fp:
            line = fp.readline()
            time = line.split(',')
            video = sys.argv[1]
            vidcap = cv2.VideoCapture(video)
            fps = vidcap.get(cv2.CAP_PROP_FPS)  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
            frame_count = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
            if fps:
                duration = frame_count / fps
            else:
                duration = 0
            time.append(duration)
            while vidcap.isOpened():
                cnt = 0
                for item in time:
                    if cnt:
                        r = g = b = count = 0
                        while vidcap.isOpened() and vidcap.get(cv2.CAP_PROP_POS_MSEC) < get_sec(item) * 1000:
                            success, image = vidcap.read()
                            if success:
                                col = getColorFreq(image)
                                r += col[1][0]
                                g += col[1][1]
                                b += col[1][2]
                                count += 1
                            else:
                                break
                            r = round(r / count, 0)
                            g = round(g / count, 0)
                            b = round(b / count, 0)
                            arr = [r, g, b]
                            print str(arr)[1:-1]
                            cnt += 1
                            cv2.destroyAllWindows()
                            vidcap.release()
