def get_sec(time_str):
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + float(s)

def getLength(time):
    cnt = 0
    for item in time:
        if cnt:
            ret = get_sec(item)
        cnt += 1
    return ret


filepath = 'movie-Scenes.csv'
with open(filepath) as fp:
    sec = 10
    line = fp.readline()
    time = line.split(',')
    length = getLength(time)
    i = sec
    rest = 0
    while i < length:
        cnt = 0
        speed = rest
        prev = 0
        for item in time:
            if cnt:
                scene = get_sec(item)
                if i < scene:
                    speed += (i - prev) / (scene - prev)
                    rest = 1 - (i - prev) / (scene - prev)
                    print(speed)
                    break
                else:
                    time[cnt] = "0:0:0"
                    if scene:
                        prev = scene
                        speed += 1
            cnt += 1
        i += sec
